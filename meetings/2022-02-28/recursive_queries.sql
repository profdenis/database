set search_path to blog;

insert into "user"(nickname, email)
values ('denis', 'denis@example.com'),
       ('alice', 'alice@example.com');

insert into blogpost(title, contents, user_id)
values ('First post', 'This is my first post', 1);

insert into comment(contents, blogpost_id, user_id)
values ('I hope you will like my first post', 1, 1);

insert into comment(contents, reply_to_id, user_id)
values ('Yes I like it', 1, 2),
       ('Thank you', 2, 1);

insert into comment(contents, blogpost_id, user_id)
values ('I hate it', 1, 2);

insert into comment(contents, reply_to_id, user_id)
values ('sorry you didn''t like it', 4, 1),
       ('you''re welcome', 3, 2);

select *
from comment
where reply_to_id = 1;

select *
from comment
where blogpost_id = 1;


-- f(x) = 1 if x <= 1
-- f(x) = f(x-1)*2 if x > 1
-- f(4) = f(3)*2
-- f(4) = (f(2)*2)*2
-- f(4) = ((f(1)*2)*2)*2
-- f(4) = ((1*2)*2)*2

with recursive replies as (
    select comment_id, contents, reply_to_id, user_id
    from comment
--     where reply_to_id = 1
--     where comment_id = 4
    where blogpost_id = 1
    union
    select c.comment_id, c.contents, c.reply_to_id, c.user_id
    from comment c
             inner join replies on replies.comment_id = c.reply_to_id
)
select *
from replies;