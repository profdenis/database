# Topics covered in class

## Resources

1. W3Schools: [https://www.w3schools.com/sql/default.asp](https://www.w3schools.com/sql/default.asp)
2. SQL Zoo: [https://www.w3resource.com/sql-exercises/](https://sqlzoo.net/wiki/SQL_Tutorial)
3. SQLBolt: [https://sqlbolt.com/](https://sqlbolt.com/)
4. w3resource: [https://www.w3resource.com/sql/tutorials.php](https://www.w3resource.com/sql-exercises/)

## 2022-01-24

1. Course outline
2. Software installation
3. Introduction to SQL
4. Readings and exercises
    1. W3Schools: *SQL Intro* to *SQL Null Values*, *SQL Min and Max* to *SQL Aliases*
    2. SQL Zoo: tutorials 0 to 3
    3. SQLBolt: lessons 1 to 5

## 2022-01-25

1. Joins
2. Readings and exercises
    1. examples in `03a_joins.md`
    2. W3Schools: *SQL Joins* to *SQL Full Join*
    3. SQL Zoo: tutorials 6 and 7
    4. SQLBolt: lessons 6 to 9

## 2022-01-26

1. More joins
2. Readings and exercises

## 2022-01-28

1. Grouping
2. Readings and exercises
    1. at the end of the file `04b_grouping.md`
    2. W3Schools: *SQL Group By* and *SQL Having*
    3. SQL Zoo: tutorials 5, 8, and 8+
    4. SQLBolt: lessons 10, 11 and 12

## 2022-01-31

1. Sub-queries
2. Readings and exercises
    1. W3Schools: *SQL Exists* and *SQL Any, All*
    2. SQL Zoo: tutorial 4
    3. SQLBolt: intermediate SQL topics [https://sqlbolt.com/topics](https://sqlbolt.com/topics)

## 2022-02-01

1. Exercises (no lecture, only exercises)
    1. Football: run `football_postgres.sql` to create the tables, and the questions are in `football_queries.sql`
    2. Academics: run `academics_postgres.sql` to create the tables, the questions are in `academics_questions.md` and
       the answers in `academics_queries.sql`

## 2022-02-02

1. Pagila DB examples
2. Assignment 1

## 2022-02-04

1. Exercises and Assignment 1 (no lecture, only exercises and assignment work)

## 2022-02-07

1. ER modelling basics
2. ER modelling examples (_Contacts_, _SimpleBank_ and _College_)
3. Exercises: use the draw.io app from [https://www.diagrams.net/](https://www.diagrams.net/) (or use some other similar
   app) to practice drawing ER diagrams

## 2022-02-08

1. Assignment 1

## 2022-02-09

1. More ER modelling examples (_Music_, _Apartments_)
2. Exercises (ER modelling) and Assignment 1

## 2022-02-11

1. Exercises (ER modelling) and Assignment 1


## 2022-02-14

1. Converting an ER diagram to a relational schema
2. SQL `create table`

## 2022-02-15

1. More examples of _ER to relational_ and `create table`
2. SQL `insert into`, `update`
3. Exercises and Assignment 1

## 2022-02-16

1. Exercises and Assignment 1

## 2022-02-18

1. Full example (ER modelling, ER to relational, DB creation, ...)
2. Exercises


## 2022-02-21

1. blog and book ER modelling exercises
2. blog ER to relational

## 2022-02-22

3. book ER to relational

## 2022-02-23

1. Assignment 1 solutions

## 2022-02-25

1. Exam 1

## 2022-02-16

1. Recursive queries
2. Normalization [https://www.studytonight.com/dbms/database-normalization.php](https://www.studytonight.com/dbms/database-normalization.php)

