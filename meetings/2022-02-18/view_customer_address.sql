-- Animation: ANACONDA CONFESSIONS, Children: LANGUAGE COWBOY, Classics: COLOR PHILADELPHIA, WESTWARD SEABISCUIT, Comedy: VERTIGO NORTHWEST, Documentary: ACADEMY DINOSAUR, Family: KING EVOLUTION, SPLASH GUMP, Foreign: MULHOLLAND BEAST, Games: BULWORTH COMMANDMENTS, HUMAN GRAFFITI, Horror: ELEPHANT TROJAN, LADY STAGE, RULES HUMAN, Music: WIZARD COLDBLOODED, New: ANGELS LIFE, OKLAHOMA JUMANJI, Sci-Fi: CHEAPER CLYDE, Sports: GLEAMING JAWBREAKER
set search_path to pagila;
create view customer_address as
select c.customer_id, c.first_name, c.last_name, c2.city, c3.country
from customer c
         inner join address a on a.address_id = c.address_id
         inner join city c2 on c2.city_id = a.city_id
         inner join country c3 on c3.country_id = c2.country_id;

select country, count(customer_id)
from customer_address
group by country;

with customer_address2 as (select c.customer_id, c.first_name, c.last_name, c2.city, c3.country
           from customer c
                    inner join address a on a.address_id = c.address_id
                    inner join city c2 on c2.city_id = a.city_id
                    inner join country c3 on c3.country_id = c2.country_id)
select country, count(customer_id)
from customer_address2
group by country;