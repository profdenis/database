Contact(CONTACT_ID, name, phone, address, email)
Call(CALL_ID, phone, date, time, contact_id*)

Customer(CUST_ID, name, address, phone)
Account(ACCOUNT_NUMBER, type, balance)
Transaction(TRANS_ID, code, amount, date, time, description, account_number*)
CustomerAccount(cust_id*, account_number*)