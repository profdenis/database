create schema contacts2;
set search_path to contacts2;

drop table if exists contact;
create table if not exists contact
(
    contact_id integer generated always as identity primary key,
    name       char(50) not null,
    phone      varchar(255) not null,
    address    text,
    email      text
);

drop table if exists call;
create table if not exists call
(
    call_id integer generated always as identity primary key,
    phone   text,
--   date date,
--   time time,
    datetime    timestamp not null,
    contact_id integer references contact(contact_id)
);