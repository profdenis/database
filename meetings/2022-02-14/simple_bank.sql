-- drop schema simple_bank cascade;
create schema if not exists simple_bank;
set search_path to simple_bank;

drop table if exists customer_account;
drop table if exists transaction;
drop table if exists account;
drop table if exists customer;

create table if not exists customer (
    cust_id integer generated always as identity primary key,
    name text not null,
    address text not null,
    phone text not null
);


create table if not exists account (
    account_number integer generated always as identity primary key,
    type char(3) not null, -- CHQ or SAV or ...
    balance decimal(12, 2) not null
);


create table if not exists transaction (
    trans_id integer generated always as identity primary key,
    code char(4) not null,
    amount decimal(12, 2) not null,
    datetime timestamp not null,
    description text,
    account_number integer references account(account_number) not null
);


create table if not exists customer_account (
    cust_id integer references customer(cust_id) not null,
    account_number integer references account(account_number) not null
);