set search_path to pagila;

-- 1. Find all customers living in Canada. Give the first name, the last name, the email of each customer. Sort by last
--    names and then by first names (both increasing order).
select cu.first_name, cu.last_name, cu.email
from customer cu
         inner join address a on a.address_id = cu.address_id
         inner join city on city.city_id = a.city_id
         inner join country co on co.country_id = city.country_id
where country = 'Canada'
order by last_name, first_name;

-- 2. How many employees (staff) are living in Canada?
select count(staff_id) as n_staff_canada
from staff s
         inner join address a on a.address_id = s.address_id
         inner join city on city.city_id = a.city_id
         inner join country co on co.country_id = city.country_id
where country = 'Canada';

-- 3. Find the city and the country of the customer with this email : `SHARON.ROBINSON@sakilacustomer.org`
select city.city, co.country
from customer cu
         inner join address a on a.address_id = cu.address_id
         inner join city on city.city_id = a.city_id
         inner join country co on co.country_id = city.country_id
where cu.email = 'SHARON.ROBINSON@sakilacustomer.org';

-- 4. Find the number of customers for each country.
select co.country_id, co.country, count(cu.customer_id) as n_customers
from customer cu
         inner join address a on a.address_id = cu.address_id
         inner join city on city.city_id = a.city_id
         inner join country co on co.country_id = city.country_id
group by co.country_id, co.country;

-- 5. Find the customers who have never rented any film. Give the first name, the last name and the email of these
--    customers.
select cu.first_name, cu.last_name, cu.email
from customer cu
         left join rental r on cu.customer_id = r.customer_id
where r.rental_id is null;

-- 6. Find the customers with rentals that haven't been returned yet. Give the first name, the last name and the email of
--    these customers.
select distinct cu.first_name, cu.last_name, cu.email
from customer cu
         inner join rental r on cu.customer_id = r.customer_id
where r.return_date is null;

-- 7. Find the number of rentals for each customer. Sort the results by decreasing number of rentals. Keep only the
--    customers with at least 30 rentals. Give the `customer_id`, the email and the number of rentals of each such
--    customer.
select cu.customer_id, cu.email, count(r.rental_id) as n_rentals
from customer cu
         inner join rental r on cu.customer_id = r.customer_id
group by cu.customer_id, cu.email
having count(r.rental_id) >= 30
order by n_rentals desc;

-- 8. Find the customers with less than 15 rentals. Give only the `customer_id` and the email of each such customer.
select cu.customer_id, cu.email
from customer cu
         left join rental r on cu.customer_id = r.customer_id
group by cu.customer_id, cu.email
having count(r.rental_id) < 15;

-- 9. Find the total amount of all payments made. Round the total to 2 decimal digits.
select round(sum(amount), 2)
from payment;

-- 10. Find the total amount of all payments made each day. The days without any payments should **not** be listed. You
--     can use the `date` function to extract the date from a `timestamp` column. Round the totals to 2 decimal digits.
select date(payment_date) as date, round(sum(amount), 2) as total_payments
from payment
group by date(payment_date)
order by date;

-- 11. Find the number of times the film `CONNECTION MICROCOSMOS` has been rented.
select count(r.rental_id) as n_rentals
from film
         inner join inventory i on film.film_id = i.film_id
         inner join rental r on i.inventory_id = r.inventory_id
where title = 'CONNECTION MICROCOSMOS';

-- 12. Find the films that have never been rented. Give the `film_id` and title of each such film.
select film.film_id, film.title
from film
         left join inventory i on film.film_id = i.film_id
         left join rental r on i.inventory_id = r.inventory_id
group by film.film_id, film.title
having count(r.rental_id) = 0;

-- doesn't work because 1 copy of film 'ACADEMY DINOSAUR' in the inventory has never been rented
select distinct film.film_id, film.title, r.rental_id
from film
         left join inventory i on film.film_id = i.film_id
         left join rental r on i.inventory_id = r.inventory_id
where r.rental_id is null;

-- 13. Find the number of times each film has been rented. Give the `film_id` and title of each such film in addition to
--     the number of rentals. Sort in decreasing order of the number of rentals. Don't forget films that have never been
--     rented. Make sure your result don't contradict your results from the previous question.
select film.film_id, film.title, count(r.rental_id) as n_rentals
from film
         left join inventory i on film.film_id = i.film_id
         left join rental r on i.inventory_id = r.inventory_id
group by film.film_id, film.title
order by n_rentals desc;

-- 14. Find all customers and all employees (staff) living in Canada. Give the first name, last name and email of theses
--     people. Sort by family names then by first names.
select cu.first_name, cu.last_name, cu.email
from customer cu
         inner join address a on a.address_id = cu.address_id
         inner join city on city.city_id = a.city_id
         inner join country co on co.country_id = city.country_id
where country = 'Canada'
union
select s.first_name, s.last_name, s.email
from staff s
         inner join address a on a.address_id = s.address_id
         inner join city on city.city_id = a.city_id
         inner join country co on co.country_id = city.country_id
where country = 'Canada'
order by last_name, first_name;

with people as (select first_name, last_name, email, address_id
                from staff
                union
                select first_name, last_name, email, address_id
                from customer)
select first_name, last_name, email
from people p
         inner join address a on a.address_id = p.address_id
         inner join city on city.city_id = a.city_id
         inner join country co on co.country_id = city.country_id
where country = 'Canada'
order by last_name, first_name;

-- 15. Find the films not currently in stock (i.e. the films not in the inventory). Give the `film_id`, the title and the
--     release year of these movies. Sort by release year (increasing order).
select f.film_id, title, release_year
from film f
         left join inventory i on f.film_id = i.film_id
where inventory_id is null
order by release_year;


-- Challenge
-- 1. Find the average length of all films. Present the results on 3 columns (each column must be an integer) :
--    `hours`, `minutes` and `seconds`.

-- this query just for testing
select avg(length)                             as average,
       (avg(length) / 60)                      as hours,
       avg(length) % 60                        as minutes,
       (avg(length) - floor(avg(length))) * 60 as seconds
from film;

select floor(avg(length) / 60)                        as hours,
       floor(avg(length) % 60)                        as minutes,
       round((avg(length) - floor(avg(length))) * 60) as seconds
from film;


-- 2. Find the customers with the largest number of rentals. Give the `customer_id`, first name, last name and the
--    number of rentals of each such customer.
with n_rentals_per_customer as (select customer_id, count(rental_id) as n_rentals
                                from rental
                                group by customer_id),
     max_rentals as (select max(n_rentals)
                     from n_rentals_per_customer)
select n.customer_id, c.first_name, c.last_name, n_rentals
from n_rentals_per_customer n
         inner join customer c on n.customer_id = c.customer_id
where n_rentals = (select * from max_rentals);

-- 3. Find the total payment amount of each customers. Give the `customer_id`, first name, last name in addition to the
--    total amount for each customer. Don't forget the customers without any rentals, their total amount should not
--    be `null`, it should be 0. Round the results to 2 decimal digits. Sort the results by the total amount (decreasing
--    order).
select c.customer_id, c.first_name, c.last_name, coalesce(round(sum(amount), 2), 0) as total_payment
from customer c
         left join payment p on c.customer_id = p.customer_id
group by c.customer_id
order by total_payment desc;

-- 4. Find the customers with a total payment amount lower than the average. Give the `customer_id`, first name, last name
--    in addition to the total amount for each customer. Don't forget the customers without any rentals, their total amount
--    should not be `null`, it should be 0. Round the results to 2 decimal digits. Sort the results by the total amount (
--    increasing order).
with total_payment_per_customer as
         (select c.customer_id, c.first_name, c.last_name, coalesce(round(sum(amount), 2), 0) as total_payment
          from customer c
                   left join payment p on c.customer_id = p.customer_id
          group by c.customer_id)
select customer_id, first_name, last_name, total_payment
from total_payment_per_customer m
where total_payment < (select avg(total_payment)
                       from total_payment_per_customer)
order by total_payment;


-- 5. Find the films in stock in exactly 1 store. Give the `film_id`, the title and release year of these films. Sort by
--    the release year (increasing order).
select f.film_id, title, release_year
from film f
         inner join inventory i on f.film_id = i.film_id
group by f.film_id, title, release_year
having count(distinct store_id) = 1
order by release_year;

with in_stock as (select i.*
                  from inventory i
                  except
                  select i.*
                  from inventory i
                           inner join rental r on r.inventory_id = i.inventory_id
                  where return_date is null)
select f.film_id, title, release_year
from film f
         inner join in_stock i on f.film_id = i.film_id
group by f.film_id, title, release_year
having count(distinct store_id) = 1
order by release_year;

-- 6. Find the films in stock in all the stores. Give the `film_id`, the title and release year of these films. Sort by
--    the release year (increasing order).
select f.film_id, title, release_year
from film f
         inner join inventory i on f.film_id = i.film_id
group by f.film_id, title, release_year
having count(distinct store_id) = (select count(store_id) from store)
order by release_year;

with in_stock as (select i.*
                  from inventory i
                  except
                  select i.*
                  from inventory i
                           inner join rental r on r.inventory_id = i.inventory_id
                  where return_date is null)
select f.film_id, title, release_year
from film f
         inner join in_stock i on f.film_id = i.film_id
group by f.film_id, title, release_year
having count(distinct store_id) = (select count(store_id) from store)
order by release_year;

